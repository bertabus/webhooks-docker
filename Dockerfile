FROM almir/webhook
RUN  apk --update --upgrade add git go docker curl bash python openssh-client && \
#     curl -L -o /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py && \
#     python /tmp/get-pip.py && \
#     pip install docker-compose && \
#     rm -f /tmp/get-pip.py && \
     rm -rf /var/cache/apk/*
# /etc/webhooks is a virtual volume, has permission issues. Use /etc/hooks instead
COPY hooks.json.example /etc/hooks/hooks.json
COPY scripts/ /etc/hooks/scripts/
RUN chmod +x -R /etc/hooks/scripts
CMD ["-verbose", "-hooks=/etc/hooks/hooks.json", "-hotreload"]
