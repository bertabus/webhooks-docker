# webhooks-docker

docker image for performing webhooks.

Mostly for performing "go gets" or "git pulls"

create with 
```sh
docker run --name webhooks \
 --restart always -d -p 9092:9000 \
 -v /srv:/srv \
 -v /root/.ssh:/root/.ssh:ro \
 -v /etc/localtime:/etc/localtime:ro \
 registry.gitlab.com/bertabus/webhooks-docker
```

